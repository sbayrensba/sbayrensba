﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SportsManager
{
    /// <summary>
    /// Summary description for ArbitrageCalculator
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ArbitrageCalculator : System.Web.Services.WebService
    {
        [WebMethod]
        public string Arbitrage_Three(double odd_1, double odd_X, double odd_2, int limit_each_price)
        {
            return calc_Arbitrage_Three(odd_1, odd_X, odd_2, limit_each_price);
        }

        [WebMethod]
        public string Arbitrage_Two(double odd_1, double odd_2, int limit_each_price)
        {
            return calc_Arbitrage_Two(odd_1, odd_2, limit_each_price);      
        }

        public string calc_Arbitrage_Three(double odd_1, double odd_X, double odd_2, int limit_each_price)
        {
            XYZ_Pair xyz_Pair = new XYZ_Pair();

            /* 
                 * odd_1 : x$
                 * odd_X : y$ 
                 * odd_2 : z$
            */

            double a = odd_1 - 1;
            double b = odd_X - 1;
            double c = odd_2 - 1;

            /* 
                 * arbitrage conditions: 
                 * c > (a+b+2)/(a*b-1)
                 * c > 1/a
                 * b > 1/a
            */

            int x = 0;
            int y = 0;
            int z = 0;

            bool found_Arbitrage = false;

            if (c > (a + b + 2) / (a * b - 1) && c > 1 / a && b > 1 / a)
            {
                // (c+1)*x/(b*c-1) < y < (a*c-1)*x/(c+1)
                // (x+y)/c < z < minimum(a*x-y,b*y-x)

                double worstcaseprofit = 0;

                for (x = 1; x <= limit_each_price; x++)
                {
                    double y_lowerLimit = (c + 1) * x / (b * c - 1);
                    double y_upperLimit = (a * c - 1) * x / (c + 1);
                    int y_lowerCeiling = Convert.ToInt32(Math.Ceiling(y_lowerLimit));
                    int y_upperFloor = Convert.ToInt32(Math.Floor(y_upperLimit));
                    if (y_lowerCeiling <= y_upperFloor)
                    {
                        //there is an integer for y price
                        for (int y_temp = y_lowerCeiling; y_temp <= Minimun(y_upperFloor, limit_each_price); y_temp++)
                        {
                            double z_lowerLimit = (x + y_temp) / c;
                            double z_upperLimit = Minimun(a * x - y_temp, b * y_temp - x);
                            int z_lowerCeiling = Convert.ToInt32(Math.Ceiling(z_lowerLimit));
                            int z_upperFloor = Convert.ToInt32(Math.Floor(z_upperLimit));
                            if (z_lowerCeiling < z_upperFloor)
                            {
                                y = y_temp;
                                for (int z_temp = z_lowerCeiling; z_temp <= Minimun(z_upperFloor, limit_each_price); z_temp++)
                                {
                                    z = z_temp;
                                    found_Arbitrage = true;

                                    double curr_worstcaseprofit = GetWorstCaseProfit(a * x - y - z, b * y - x - z, c * z - x - y);
                                    if (curr_worstcaseprofit > worstcaseprofit)
                                    {
                                        worstcaseprofit = curr_worstcaseprofit;
                                        xyz_Pair.x = x;
                                        xyz_Pair.y = y;
                                        xyz_Pair.z = z;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!found_Arbitrage)
            {
                xyz_Pair.x = 0;
                xyz_Pair.y = 0;
                xyz_Pair.z = 0;
            }
            return xyz_Pair.x.ToString() + "," + xyz_Pair.y.ToString() + "," + xyz_Pair.z.ToString();
        }

        public string calc_Arbitrage_Two(double odd_1, double odd_2, int limit_each_price)
        {
            XZ_Pair xz_Pair = new XZ_Pair();

            /* 
                 * odd_1 : x$
                 * odd_2 : z$
            */

            double a = odd_1 - 1;
            double c = odd_2 - 1;

            /* 
                 * arbitrage conditions: 
                 * a * c > 1
            */

            int x = 0;
            int z = 0;

            bool found_Arbitrage = false;

            double worstcaseprofit = 0;

            if (a * c > 1)
            {
                // (1/c)*x < z < a*x
                for (x = 1; x <= limit_each_price; x++)
                {
                    double z_lowerLimit = (1 / c) * x;
                    double z_upperLimit = a * x;
                    int z_lowerCeiling = Convert.ToInt32(Math.Ceiling(z_lowerLimit));
                    int z_upperFloor = Convert.ToInt32(Math.Floor(z_upperLimit));
                    if (z_lowerCeiling < z_upperFloor)
                    {
                        for (int z_temp = z_lowerCeiling; z_temp <= Minimun(z_upperFloor, limit_each_price); z_temp++)
                        {
                            z = z_temp;
                            found_Arbitrage = true;
                            // (x,z) is a possible pair
                            double curr_worstcaseprofit = GetWorstCaseProfit(a * x - z, c * z - x);
                            if (curr_worstcaseprofit > worstcaseprofit)
                            {
                                worstcaseprofit = curr_worstcaseprofit;
                                xz_Pair.x = x;
                                xz_Pair.z = z;
                            }
                        }
                    }
                }
            }

            if (!found_Arbitrage)
            {
                xz_Pair.x = 0;
                xz_Pair.z = 0;
            }
            return xz_Pair.x.ToString() + "," + xz_Pair.z.ToString();
        }


        struct XZ_Pair
        {
            public int x;
            public int z;
        }

        struct XYZ_Pair
        {
            public int x;
            public int y;
            public int z;
        }

        static double Minimun(double m, double n)
        {
            if (m < n) return m;
            else return n;
        }

        static double Maximum(double m, double n)
        {
            if (m > n) return m;
            else return n;
        }


        double GetWorstCaseProfit(double m, double n)
        {
            return Minimun(m, n);
        }

        double GetWorstCaseProfit(double m, double n, double o)
        {
            return Minimun(m, Minimun(n, o));
        }


        double GetBestCaseProfit(double m, double n)
        {
            return Maximum(m, n);
        }

        double GetBestCaseProfit(double m, double n, double o)
        {
            return Maximum(m, Maximum(n, o));
        }


    }
}

