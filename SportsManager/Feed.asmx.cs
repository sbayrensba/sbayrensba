﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace SportsManager
{
    /// <summary>
    /// Summary description for Feed
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Feed : System.Web.Services.WebService
    {
        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string FeedData(int agent, string feed_data)
        {
            Application["FeedData" + agent.ToString()] += feed_data + "###";

            dynamic obj = Newtonsoft.Json.Linq.JObject.Parse(feed_data);
            dynamic href = obj.href.Value;
            dynamic val1 = float.Parse(obj.val_1.Value, CultureInfo.InvariantCulture.NumberFormat);
            dynamic val2 = float.Parse(obj.val_2.Value, CultureInfo.InvariantCulture.NumberFormat);
            dynamic valX = float.Parse(obj.val_X.Value, CultureInfo.InvariantCulture.NumberFormat);
            dynamic team_1 = obj.team_1.Value;
            dynamic team_2 = obj.team_2.Value;            
            dynamic game = obj.game.Value;
            dynamic date = obj.data.Value;

            Newtonsoft.Json.Linq.JArray overUnders = obj.overUnders;

            List<OverUnderDelta> OverUnderDeltas = new List<OverUnderDelta>();

            try
            {
                for (int j = 0; j < overUnders.Count; j++)
                {
                    OverUnderDelta overUnderDelta = new OverUnderDelta();
                    Newtonsoft.Json.Linq.JToken curr_overUnders = overUnders[j];
                    dynamic curr_overUnders_str = Newtonsoft.Json.Linq.JObject.Parse(curr_overUnders.ToString());
                    dynamic limit = float.Parse(curr_overUnders_str.limit.Value, CultureInfo.InvariantCulture.NumberFormat);
                    dynamic over = float.Parse(curr_overUnders_str.over.Value, CultureInfo.InvariantCulture.NumberFormat);
                    dynamic under = float.Parse(curr_overUnders_str.under.Value, CultureInfo.InvariantCulture.NumberFormat);
                    overUnderDelta.date = date;
                    overUnderDelta.eventName = game + "_" + limit;
                    overUnderDelta.eventTitle = game + " (Over Under " + limit + ")";
                    overUnderDelta.eventData = new OverUnderDetails();
                    overUnderDelta.eventData.href = href;
                    overUnderDelta.eventData.limit = limit;
                    overUnderDelta.eventData.over = over;
                    overUnderDelta.eventData.under = under;
                    overUnderDelta.eventData.teamOver = team_1;
                    overUnderDelta.eventData.teamUnder = team_2;
                    OverUnderDeltas.Add(overUnderDelta);
                }
            }
            catch (Exception e2) {
                
            }

            //Application[agent] = mydata;
            //return mydata;
            ///////////EventDelta eventDelta = mydata;//mydata must be in format of easy set to EventDelta object
            EventDelta eventDelta = new EventDelta();
            eventDelta.date = date;
            eventDelta.href = href;
            eventDelta.eventName = game;
            eventDelta.eventTitle = team_1 + "-" + team_2;
            eventDelta.eventDate = date;
            eventDelta.eventData = new Dictionary<int, float>();
            eventDelta.eventData.Add(1,val1);
            eventDelta.eventData.Add(2, val2);
            eventDelta.eventData.Add(3, valX);

            FullFeedData ffd = new FullFeedData();
            ffd.BookmakerData = GetAllData();//of all agents for each in enumBookmakers
            ffd.BookmakerOverUnderData = GetAllOverUnderData();

            List<Arbitraz> arb = ffd.UpdateFeedData(Convert.ToInt32(agent), eventDelta, OverUnderDeltas);
            List<Arbitraz> arbOverUnder = null;
            try
            {
                arbOverUnder = ffd.GetOverUnderArbitrazList(Convert.ToInt32(agent), eventDelta, OverUnderDeltas);
            }
            catch (Exception e2)
            {
            }
            var json = new JavaScriptSerializer().Serialize(arb);

            try
            {
                var jsonOverUnder = new JavaScriptSerializer().Serialize(arbOverUnder);
                Application["overUnderArbitrazlist"] = jsonOverUnder; //agent -> from enumBookmakers 
                Application["overunder_" + agent.ToString()] = ffd.BookmakerOverUnderData[agent];
            }
            catch (Exception e2)
            {
            }

            Application["arbitrazlist"] = json; //agent -> from enumBookmakers             

            Application[agent.ToString()] = ffd.BookmakerData[agent];

            return "o.k.";
        } 

        [WebMethod(enableSession: true)]
        public string GetFeedData(int agent)
        {
            if (Application["FeedData" + agent.ToString()] == null) return "";
            return Application["FeedData" + agent.ToString()].ToString();
        }
        
        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetArbitrazList()
        {
            if (Application["arbitrazlist"] == null) return "";
            return Application["arbitrazlist"].ToString();
        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetOverUnderArbitrazList()
        {
            if (Application["overUnderArbitrazlist"] == null) return "";
            return Application["overUnderArbitrazlist"].ToString();
        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetData(string agent)
        {
            return Application[agent].ToString();
        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ClearTable()
        {
            Application.Clear();
            FullFeedData.ArbitrazList.Clear();
            FullFeedData.OverUnderArbitrazList.Clear();
        }

        public Dictionary<int, Dictionary<string, SingleEvent>> GetAllData()
        {
            Dictionary<int, Dictionary<string, SingleEvent>> data = new Dictionary<int, Dictionary<string, SingleEvent>>();
            var values = Enum.GetValues(typeof(enumBookmakers));
            foreach (int agent in values) {
                data[agent] = (Dictionary<string, SingleEvent>)Application[agent.ToString()];
            }
            return data;
        }

        public Dictionary<int, Dictionary<string, SingleOverUnderEvent>> GetAllOverUnderData()
        {
            Dictionary<int, Dictionary<string, SingleOverUnderEvent>> data = new Dictionary<int, Dictionary<string, SingleOverUnderEvent>>();
            var values = Enum.GetValues(typeof(enumBookmakers));
            foreach (int agent in values)
            {
                data[agent] = (Dictionary<string, SingleOverUnderEvent>)Application["overunder_" + agent.ToString()];
            }
            return data;
        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string RemoveOldData(string agent)
        {
            FullFeedData ffd = new FullFeedData();
            ffd.BookmakerData = GetAllData();//of all agents for each in enumBookmakers
            ffd.DeleteEvents();
            return "o.k.";
        }
         

        [WebMethod(enableSession: true)]
        public string GetTranslate()
        {
            return Application["teamNames"].ToString();
        }

        [WebMethod(enableSession: true)]
        public void AddTranslate(string teamName,string href)
        {
            if (Application["teamNames"] != null && Application["teamNames"].ToString().Contains(teamName)) return;
            Application["teamNames"] +=  WebUtility.HtmlEncode("<div>" + teamName + " ( " + href + " ) " + "</div>");
        }

        [WebMethod(enableSession: true)]
        public void ApplicationClear()
        {
            Application["arbitrazlist"] = null;
            Application["overUnderArbitrazlist"] = null;
        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ReadDictionary()
        {
            System.Net.WebClient client = new System.Net.WebClient();
            string downloadString = client.DownloadString("https://docs.google.com/spreadsheets/d/1M3vfQNMHtfcyVAHJu4RFzamz8mQ-8_6MtXJ5TOMtd6o/pub?gid=2023062918&single=true&output=csv");
            return downloadString;
        }

    }
}
