using System;
using System.Collections.Generic;

public class FullFeedData
{
    public Dictionary<int, Dictionary<string, SingleEvent>> BookmakerData;
    public Dictionary<int, Dictionary<string, SingleOverUnderEvent>> BookmakerOverUnderData;
    public static List<Arbitraz> ArbitrazList = new List<Arbitraz>();
    public static List<Arbitraz> OverUnderArbitrazList = new List<Arbitraz>();    

    public List<Arbitraz> UpdateFeedData(int bookmakerId, EventDelta eventDelta, List<OverUnderDelta> overUnderDeltas)
    {
        UpdateBookmakerData(bookmakerId, eventDelta, overUnderDeltas);
        return GetArbitrazList(bookmakerId, eventDelta);
    }

    public List<Arbitraz> GetOverUnderArbitrazList(int bookmakerId, EventDelta eventDelta, List<OverUnderDelta> overUnderDeltas)
    {
        return GetOverUnderArbitrazList(bookmakerId, overUnderDeltas);
    }

    public void UpdateBookmakerData(int bookmakerId, EventDelta eventDelta, List<OverUnderDelta> overUnderDeltas)
    {
        foreach (var item in eventDelta.eventData)
        {
            AddOrUpdateEvent(bookmakerId, eventDelta.href,eventDelta.eventTitle, eventDelta.eventName, eventDelta.eventDate, item.Key, item.Value);
        }

        foreach (var item in overUnderDeltas)
        {
            AddOrUpdateOverUnderEvent(bookmakerId, item.eventData, item.eventName, item.eventTitle);
        }
    }
    public void AddOrUpdateEvent(int bookmakerId, string href, string eventTitle, string eventName, string eventDate, int eventType, float eventValue)
    {
        bool isNull = false;
        SingleEvent singleEvent;
        try
        {
            singleEvent = BookmakerData[bookmakerId][eventName];
        }
        catch (Exception e2) {
            isNull = true;
        }

        if (isNull)
        {
            SingleEvent se = new SingleEvent();
            se.href = href;
            se.eventTitle = eventTitle;
            se.eventName = eventName;
            se.lastUpdated = "";
            se.eventDate = eventDate;
            se.eventData = new Dictionary<int, float>();
            se.eventData.Add(eventType, eventValue);
            if (BookmakerData[bookmakerId] == null)
            {
                BookmakerData[bookmakerId] = new Dictionary<string, SingleEvent>();
            }
            BookmakerData[bookmakerId].Add(eventName, se);
        }
        else if (!BookmakerData[bookmakerId][eventName].eventData.ContainsKey(eventType))
        {
            BookmakerData[bookmakerId][eventName].eventData.Add(eventType, eventValue);
        }
        else
        {
            BookmakerData[bookmakerId][eventName].eventData[eventType] = eventValue;
        }
    }

    public void AddOrUpdateOverUnderEvent(int bookmakerId, OverUnderDetails eventData, string eventName, string eventTitle)
    {
        bool isNull = false;
        SingleOverUnderEvent singleEvent;
        try
        {
            singleEvent = BookmakerOverUnderData[bookmakerId][eventName];
        }
        catch (Exception e2) {
            isNull = true;
        }

        if (isNull)
        {
            SingleOverUnderEvent se = new SingleOverUnderEvent();
            se.href = eventData.href;
            se.eventTitle = eventTitle;
            se.eventName = eventName;
            se.lastUpdated = "";
            se.eventData = new Dictionary<string, float>();
            se.eventData.Add("Over" + eventData.limit, eventData.over);
            se.eventData.Add("Under" + eventData.limit, eventData.under);
            if (BookmakerOverUnderData[bookmakerId] == null)
            {
                BookmakerOverUnderData[bookmakerId] = new Dictionary<string, SingleOverUnderEvent>();
            }
            BookmakerOverUnderData[bookmakerId].Add(eventName, se);
        }
        else if (!BookmakerOverUnderData[bookmakerId][eventName].eventData.ContainsKey("Over" + eventData.limit))
        {
            BookmakerOverUnderData[bookmakerId][eventName].eventData.Add("Over" + eventData.limit, eventData.over);
            BookmakerOverUnderData[bookmakerId][eventName].eventData.Add("Under" + eventData.limit, eventData.under);
        }
        else
        {
            BookmakerOverUnderData[bookmakerId][eventName].eventData["Over" + eventData.limit] = eventData.over;
            BookmakerOverUnderData[bookmakerId][eventName].eventData["Under" + eventData.limit] = eventData.under;
        }
    }

    public void DeleteEvents()
    {
        //foreach (var bookmaker in BookmakerData)
        //{
        //    foreach (var eventName in bookmaker.Values)
        //    {
        //        foreach (var singleEvent in eventName.Values)
        //        {
        //            if (singleEvent.eventDate < string.Now())
        //            {
        //                bookmaker.Remove(eventName.Key);
        //            }
        //        }
        //    }
        //}
    }
    public List<Arbitraz> GetArbitrazList(int bookmakerId, EventDelta eventDelta)
    {
        string eventName = eventDelta.eventName;
        string eventTitle = eventDelta.eventTitle;
        string date = eventDelta.date;
        string href_1 = eventDelta.href;
        string href_2 = eventDelta.href;
        string href_X = eventDelta.href;
        float max_1 = eventDelta.eventData[(int)enumEventTypes.Win1];
        float max_2 = eventDelta.eventData[(int)enumEventTypes.Win2];
        float max_X = eventDelta.eventData[(int)enumEventTypes.X];

        int max_1_agent = bookmakerId;
        int max_X_agent = bookmakerId;
        int max_2_agent = bookmakerId;

        foreach (KeyValuePair<int, Dictionary<string, SingleEvent>> bookmaker in BookmakerData)
        {
            if (bookmaker.Key == bookmakerId) continue;
            Dictionary<string, SingleEvent> dict = bookmaker.Value;
            if (dict == null || !dict.ContainsKey(eventName)) continue;
            SingleEvent se = dict[eventName];
            float val_1 = se.eventData[(int)enumEventTypes.Win1];
            float val_2 = se.eventData[(int)enumEventTypes.Win2];
            float val_X = se.eventData[(int)enumEventTypes.X];

            if (val_1>max_1){
                href_1 = bookmaker.Value[eventName].href;
                max_1 = val_1;
                max_1_agent = bookmaker.Key;
                //href_1 = 
            }
            if (val_X>max_X){
                href_X = bookmaker.Value[eventName].href;
                max_X = val_X;
                max_X_agent = bookmaker.Key;
            }
            if (val_2>max_2){
                href_2 = bookmaker.Value[eventName].href;
                max_2 = val_2;
                max_2_agent = bookmaker.Key;
            }         
        }

        bool isArbitrage = Arbitrage_Three(max_1, max_X, max_2);

        if (isArbitrage)
        {
            Arbitraz arb = new Arbitraz();
            arb.date = date;
            arb.arbitrazType = "1X2";
            arb.eventTitle = eventTitle;
            arb.eventName = eventName;
            ArbitrazDetails details_1 = new ArbitrazDetails();
            details_1.href = href_1;
            details_1.bookmakerId = max_1_agent;            
            details_1.coefficient = max_1;
            ArbitrazDetails details_X = new ArbitrazDetails();
            details_X.href = href_X;
            details_X.bookmakerId = max_X_agent;
            details_X.coefficient = max_X;
            ArbitrazDetails details_2 = new ArbitrazDetails();
            details_2.href = href_2;
            details_2.bookmakerId = max_2_agent;
            details_2.coefficient = max_2;

            arb.firstTeam = details_1;
            arb.secondTeam = details_2;
            arb.xTeam = details_X;

            bool alreadyExists = false;
            for (int i = 0; i < ArbitrazList.Count; i++)
            {
                if (ArbitrazList[i].eventName == arb.eventName) 
                    alreadyExists = true;
            }

            if (!alreadyExists)
                ArbitrazList.Add(arb);
        }

        /*
        1. check lastUpdated of bookmaker2, if more then XXX continue to bookmaker3
        2. find eventDelta.eventName in bookmaker2, if not exist continue to bookmaker3
        3. find eventDelta.eventName.eventData in bookmaker2, if not exist continue to another eventData
        4. check arbitraz(eventDelta.eventName.eventData.eventValue,bookmaker2.eventName.eventData.eventValue)
        5. if arbitraz, add to list of arbitraz class and continue
        6. return list of arbitraz
        */
        return ArbitrazList;
    }

    public List<Arbitraz> GetOverUnderArbitrazList(int bookmakerId, List<OverUnderDelta> overUnderDeltas)
    {
        for (int j = 0; j < overUnderDeltas.Count; j++)
        {
            OverUnderDelta eventDelta = overUnderDeltas[j];
            string date = eventDelta.date;
            string eventName = eventDelta.eventName;
            string eventTitle = eventDelta.eventTitle;
            string href_over = eventDelta.eventData.href;
            string href_under = eventDelta.eventData.href;
            float max_over = eventDelta.eventData.over;
            float max_under = eventDelta.eventData.under;

            int max_over_agent = bookmakerId;
            int max_under_agent = bookmakerId;

            foreach (KeyValuePair<int, Dictionary<string, SingleOverUnderEvent>> bookmaker in BookmakerOverUnderData)
            {
                if (bookmaker.Key == bookmakerId) continue;
                Dictionary<string, SingleOverUnderEvent> dict = bookmaker.Value;
                if (dict == null || !dict.ContainsKey(eventName)) continue;
                SingleOverUnderEvent se = dict[eventName];
                float val_1 = se.eventData["Over" + eventDelta.eventData.limit];
                float val_2 = se.eventData["Under" + eventDelta.eventData.limit];

                if (val_1 > max_over)
                {
                    href_over = bookmaker.Value[eventName].href;
                    max_over = val_1;
                    max_over_agent = bookmaker.Key;
                }
                if (val_2 > max_under)
                {
                    href_under = bookmaker.Value[eventName].href;
                    max_under = val_2;
                    max_under_agent = bookmaker.Key;
                }
            }

            bool isArbitrage = Arbitrage_Two(max_over, max_under);

            if (isArbitrage)
            {
                Arbitraz arb = new Arbitraz();
                arb.date = date;
                arb.arbitrazType = "OverUnder";
                arb.eventTitle = eventTitle;
                arb.eventName = eventName;
                ArbitrazDetails details_over = new ArbitrazDetails();
                details_over.teamName = eventDelta.eventData.teamOver;
                details_over.href = href_over; 
                details_over.bookmakerId = max_over_agent;
                details_over.coefficient = max_over;
                ArbitrazDetails details_under = new ArbitrazDetails();
                details_under.teamName = eventDelta.eventData.teamUnder;
                details_under.href = href_under;
                details_under.bookmakerId = max_under_agent;
                details_under.coefficient = max_under;

                arb.firstTeam = details_over;
                arb.secondTeam = details_under;

                bool alreadyExists = false;
                for (int i = 0; i < OverUnderArbitrazList.Count; i++)
                {
                    if (OverUnderArbitrazList[i].eventName == arb.eventName)
                        alreadyExists = true;
                }

                if (!alreadyExists)
                    OverUnderArbitrazList.Add(arb);
            }
        }


        return OverUnderArbitrazList;
    }

    public bool Arbitrage_Three(double odd_1, double odd_X, double odd_2)
    {
        double a = odd_1 - 1;
        double b = odd_X - 1;
        double c = odd_2 - 1;

        if (c > (a + b + 2) / (a * b - 1) && c > 1 / a && b > 1 / a)
            return true;
        else
            return false;      
    }

    public bool Arbitrage_Two(double odd_over, double odd_under)
    {
        if ((odd_over - 1) * (odd_under - 1) > 1) 
            return true;
        else
            return false;
    }
}
