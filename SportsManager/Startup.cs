﻿using Microsoft.Owin;
using Owin;
using SportsManager;

namespace SportsManager
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}