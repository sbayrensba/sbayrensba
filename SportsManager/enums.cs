using System;
using System.Collections.Generic;

enum enumBookmakers {
    Pinnacle = 1,
    OneXbet = 2,
    Winline = 3,
    BetFiftyTwo = 4,
    OneBetTwoBet = 5,
    FiveDimes = 6,
    TheGreek = 7
}; 


enum enumEventTypes {
	Win1 = 1,
	Win2 = 2,
	X = 3
}

//enum enumArbitrazType{
//    LargestWin,
//    SmallestBigWin
//}

public struct SingleEvent{
	public string eventName; /*firstId_secondId*/
    public string href;
    public string eventTitle;
	public string lastUpdated;
	public string eventDate;
	public Dictionary<int, float> eventData; /*int:enumEventTypes; float:eventValue*/
}

public struct SingleOverUnderEvent{
	public string eventName; /*firstId_secondId*/
    public string href;
    public string eventTitle;
	public string lastUpdated;
	public string eventDate;
	public Dictionary<string, float> eventData; /*int:enumEventTypes; float:eventValue*/
}

public struct EventDelta{
    public string date;
    public string eventTitle;
    public string eventName; /*firstId_secondId*/
    public string href;
	public string eventDate;
    public Dictionary<int, float> eventData; /*int:enumEventTypes; float:eventValue*/
}

public struct OverUnderDelta
{
    public string date;
    public string eventTitle;
    public string eventName; /*firstId_secondId_limit*/ 
    public OverUnderDetails eventData; /*int:enumEventTypes; float:eventValue*/
}

public struct Arbitraz{
    public string date;
    public string eventTitle;
	public string eventName; /*firstId_secondId*/
	public int eventType; /*enumEventTypes;*/
	public string arbitrazType;
	public ArbitrazDetails firstTeam;
	public ArbitrazDetails secondTeam;
	public ArbitrazDetails xTeam;
}

public struct ArbitrazDetails{
    public string href;
	public string teamName;
    public int bookmakerId;
	public float coefficient;
	public float emount;	
}



public struct OverUnderDetails
{
    public string href;
    public float limit;
    public float over;
    public float under;
    public string teamOver;
    public string teamUnder;
}

