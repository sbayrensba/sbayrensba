﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SportsManager
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Feed feed = new Feed();
            string feed_data = "";

            feed_data = "{\"href\":\"https://www.1xbet.com/en/odds/match/soccer/italy/italy-serie-a?sport=True\",\"data\":\"06.01\",\"time\":\"02:00\",\"team_1\":\"Chievo\",\"team_2\":\"AS Roma\",\"game\":\"355_351\",\"val_1\":\"1.73\",\"val_X\":\"1.220\",\"val_2\":\"1.389\",\"overUnders\":[{}]}";
            feed.FeedData((int)enumBookmakers.OneXbet, feed_data);

            feed_data = "{\"href\":\"https://www.pinnaclesports.com/en/odds/match/soccer/italy/italy-serie-a?sport=True\",\"data\":\"06.01\",\"time\":\"02:00\",\"team_1\":\"Chievo\",\"team_2\":\"AS Roma\",\"game\":\"355_351\",\"val_1\":\"1.120\",\"val_X\":\"3.93\",\"val_2\":\"6.1\",\"overUnders\":[{}]}";
            feed.FeedData((int)enumBookmakers.Pinnacle, feed_data);
        }
    }
}